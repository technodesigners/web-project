from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from onlineclass.views import quiz1, quiz2, quiz3, quiz4, quiz5, quiz6, quiz7, quiz8, dashboard, quiz_show1
from account.views import home,get_feeds,sign_up,authenticate_user,log_out,forget_password,resend_link,change_password_url,autocomplete,index

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'web_proj.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^markdown/', include('django_markdown.urls')),
    url(r'^$',index,name='h'),
    url(r'^login/', home, name='home'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^feeds/',get_feeds),
    url(r'^signup/',sign_up),
    url(r'^activate/',authenticate_user,name='main'),
    url(r'^logout/',log_out),
    url(r'^frgpwd/',forget_password),
    url(r'^resend/',resend_link),
    url(r'^reset/',change_password_url),
    url(r'^searchuser/',autocomplete),
    url(r'^onlineclass/dashboard',dashboard),
    url(r'^onlineclass/quiz/1',quiz1),
    url(r'^onlineclass/quiz/2',quiz2),
    url(r'^onlineclass/quiz/3',quiz3),
    url(r'^onlineclass/quiz/4',quiz4),
    url(r'^onlineclass/quiz/5',quiz5),
    url(r'^onlineclass/quiz/6',quiz6),
    url(r'^onlineclass/quiz/7',quiz7),
    url(r'^onlineclass/quiz/8',quiz8),
    url(r'^onlineclass/submit(?P<quiz_no>[0-9]+)',quiz_show1),
) + static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)

urlpatterns += patterns('',
    url(r'^captcha/', include('captcha.urls')),
)
