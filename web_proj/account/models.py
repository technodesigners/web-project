from django.db import models
from django.contrib.auth.models import User
import datetime
'''from django.db import models
from django.contrib.auth.models import User
# Create your models here.
class Country(models.Model):
    country_name=models.CharField(max_length=40)
    def __self__(self):
        return self.country_name
class State(models.Model):
    country=models.ForeignKey(Country)
    state_name=models.CharField(max_length=50)
    def __self__(self):
        return self.state_name+" , "+self.country.country_name
class City(models.Model):
    country=models.ForeignKey(State)
    city_name=models.CharField(max_length=50)
    pincode=models.IntegerField()
    def __self(self):
        return self.city_name+" , "+self.state_name+" , "+self.country.country_name+" - "+self.pincode
class Progress(models.Model):
    last_quiz=models.IntegerField(default=0)
    last_tutorial=models.IntegerField(default=0)
    user=models.OneToOneField(User)
class Profile(models.Model):
    user=models.OneToOneField(User)
    profile_pic=models.ImageField(upload_to="/pics",default='/pics/generic.png')
    dob=models.DateField()
    t=((1,'Male'),(0,'Female'))
    gender=models.IntegerField(max_length=1,choices=t)
    phone=models.BigIntegerField(max_length=10)
    city=models.ManyToManyField(City)
    score=models.IntegerField(default=0)
    def __str__(self):
        return self.user.first_name'''

# Create your models here.
class Country(models.Model):
    name=models.CharField(max_length=30,unique=True)
    def __str__(self):
        return self.name

    class Meta:
        db_table='country'
        verbose_name_plural='countries'

class State(models.Model):
    country=models.ForeignKey(Country)
    name=models.CharField(max_length=50)
    def __str__(self):
        return self.country.name+">" + self.name
    class Meta:
        db_table='state'

class City(models.Model):
    state=models.ForeignKey(State)
    name=models.CharField(max_length=70)
    def __str__(self):
        return self.state.name+ ">" +self.state.country.name+ ">" +self.name
    class Meta:
        db_table='city'
        verbose_name_plural='cities'

class Progress(models.Model):
    last_quiz=models.IntegerField(default=0)
    last_tutorial=models.IntegerField(default=0)
    user=models.OneToOneField(User)
    
class UserProfile(models.Model):
    profile_pic=models.ImageField(upload_to='profile_pics/',blank=True,default='/profile_pics/generic.png')
    #phone_number=models.CharField(max_length=10,unique=True)
    user=models.OneToOneField(User)
    #dob=models.DateField(blank=True)
    dob = models.DateField(default=datetime.date.today)
    t=((1,'Male'),(0,'Female'))
    gender=models.IntegerField(max_length=1,choices=t,default=1)
    phone=models.CharField(max_length=10,default='0')
    #city=models.ManyToManyField(City)
    score=models.IntegerField(default=0)
    def __str__(self):
        return self.user.first_name
    #city=models.ForeignKey(City)
