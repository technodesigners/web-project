from django import forms
from django.contrib.auth.models import User
from account.models import UserProfile
from captcha.fields import CaptchaField
from django.forms import extras

'''class SignupForm(forms.Form):
	username=forms.CharField(label="Username",max_length=20,required=True)
	email=forms.EmailField(label="Email Id ", required=True)
	first_name=forms.CharField(label="First Name",required=False)
	last_name=forms.CharField(label="Last Name",required=False)
	password = forms.CharField(label="Password",max_length=32, required=True,widget=forms.PasswordInput)
	password1=forms.CharField(label="Password(again)",max_length=32,required=True,widget=forms.PasswordInput)

	#class Meta:
		#model=User
		#fields=['username','email','first_name','last_name']

'''
BIRTH_YEAR_CHOICES = ('1980', '1981', '1982','1983','1984', '1985', '1986','1987','1988', '1989', '1990', '1991', '1992', '1993', '1994', '1995', '1996', '1997', '1998', '1999', '2000')
class SignupForm(forms.Form):
    username=forms.CharField(label="Username",max_length=20,required=True,widget=forms.TextInput(attrs={'placeholder': 'Username'}))
    email=forms.EmailField(label="Email Id ", required=True,widget=forms.EmailInput(attrs={'placeholder': 'someone@xyz.com'}))
    first_name=forms.CharField(label="First Name",required=False,widget=forms.TextInput(attrs={'placeholder': 'abc'}))
    last_name=forms.CharField(label="Last Name",required=False,widget=forms.TextInput(attrs={'placeholder': 'xyz'}))
    password = forms.CharField(label="Password",max_length=32, required=True,widget=forms.PasswordInput(attrs={'placeholder': 'Password'}))
    password1=forms.CharField(label="Confirm Password",max_length=32,required=True,widget=forms.PasswordInput(attrs={'placeholder': 'Password'}),help_text="Should we same as password")
    dob = forms.DateField(label="Date of Birth",required=False,widget=extras.SelectDateWidget(years=BIRTH_YEAR_CHOICES))
    profile_pic=forms.ImageField(label="Profile Picture",required=False,widget=forms.FileInput())
    phone=forms.CharField(label="Phone Number",required=False,widget=forms.TextInput(attrs={'placeholder': '1111111111'}))
    captcha = CaptchaField()

    '''class Meta:
        model=UserProfile
        fields=['profile_pic']'''


class LoginForm(forms.Form):
	username=forms.CharField(initial="", required=True,widget=forms.TextInput(attrs={'placeholder': 'Username'}))
	#password=forms.CharField(initial="",required=True)
	password = forms.CharField(initial="",max_length=32, required=True,widget=forms.PasswordInput(attrs={'placeholder': 'Password'}))


class ForgetPasswordForm(forms.Form):
    username=forms.CharField(initial="",required=True,widget=forms.TextInput(attrs={'placeholder': 'Username'}))

class ResendForm(forms.Form):
    username=forms.CharField(initial="",required=True,widget=forms.TextInput(attrs={'placeholder': 'Username'}))

class PasswordChange(forms.Form):
    password = forms.CharField(label="Password",max_length=32, required=True,widget=forms.PasswordInput)
    password1=forms.CharField(label="Confirm Your Password",max_length=32, required=True,widget=forms.PasswordInput)
