from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import *

# Register your models here.
class UserInline(admin.StackedInline):
    model=UserProfile
    can_delete=False

class UserAdmin(UserAdmin):
    inlines = (UserInline,)

admin.site.unregister(User)
admin.site.register(User,UserAdmin)

admin.site.register(Country)
admin.site.register(State)
admin.site.register(City)

admin.site.register(UserProfile)
