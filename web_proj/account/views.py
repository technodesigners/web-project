from django.shortcuts import render, redirect
from .forms import SignupForm, LoginForm,ForgetPasswordForm,ResendForm,PasswordChange
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.contrib.auth.models import User
from account.models import UserProfile
import datetime
from django.core.mail import EmailMessage
from django.core.exceptions import ValidationError
import base64
import binascii
from django.core import serializers
from django.core.serializers import json
#http://127.0.0.1:8000/onlineclass/dashboard
# Create your views here.
def home(request):
    if (request.user.is_authenticated()):
        return redirect('/onlineclass/dashboard/')
    title="Learn Linux "
    body='Learn Linux!!'
    if (request.method == 'GET'):
        form = LoginForm()
        next_url = request.GET.get('next', '')
        return render(request, 'account/home.html', {'form' : form ,'next': next_url,'body':body, 'title':title})
        #return render(request, 'mylogin/name.html', {'form': form})
    #if (request.method == 'POST'):
    else :
        next_url = request.POST.get('next', '')
        form = LoginForm(request.POST)
        if form.is_valid():
            uname = form.cleaned_data['username']
            passwd = form.cleaned_data['password']
            user = authenticate(username=uname, password=passwd)
            if user is not None :
                a=User.objects.get(username=uname)
                if a.is_active==False :
                    #return HttpResponse("Confirm the Registration link !!")
                    body="Confirm the Registration by clicking on the Link sent to your Email Id"
                    return render(request, 'account/message.html', {'message': body})
                login(request, user)
                print(next_url)
                if next_url :
                    return redirect(next_url)
                return redirect('/onlineclass/dashboard/')
            # print (user.get_full_name())
            else:
                #return HttpResponse("Invalid User")
                body="Invalid User !! Either You have not registered on our website or you entering wrong username or password !! "
                return render(request, 'account/message.html', {'message': body})
        else:
            return render(request, 'account/home.html', {'form': form ,'next': next_url,'body':body, 'title':title})


def get_feeds(request):
    body=r'''There is nothing to show in your feeds'''
    link="http://127.0.0.1:8000/logout/"
    return render(request, 'account/message.html', {'message': body,'link':link,'name':'Logout' })

def index(request):
    body=r'''There is nothing to show in your feeds'''
    link="http://127.0.0.1:8000/logout/"
    return render(request, 'account/index.html', {})

def log_out(request):
    logout(request)
    return redirect('h')

def sign_up(request):
    if (request.user.is_authenticated()):
        return redirect('/feeds/')
    if(request.method=='GET'):
        form = SignupForm()
        return render(request, 'account/signup.html', {'form': form})
    if (request.method == 'POST'):
        print("Hi")
        form = SignupForm(request.POST,request.FILES)
        print("hello")
        if form.is_valid():
            print("Yo")
            uname=form.cleaned_data['username']
            pwd=form.cleaned_data['password']
            email_id=form.cleaned_data['email']
            firstname=form.cleaned_data['first_name']
            lastname=form.cleaned_data['last_name']
            pwd1=form.cleaned_data['password1']
            profile_image=form.cleaned_data['profile_pic']
            phone_no=form.cleaned_data['phone']
            dob_date=form.cleaned_data['dob']
            if pwd1 != pwd:
                #raise ValidationError("Passwords Didnot match ")
                body="Passwords Didnot Match !! Try again "
                return render(request, 'account/signup.html', {'form':form,'my_error_pwd':body})
            list=User.objects.all()
            for item in list:
                if uname==item.username:
                    #return HttpResponse("A user exists there with the same Username ")
                    body="A User exists with the same Username !! Try again with a different Username "
                    return render(request, 'account/signup.html', {'form':form,'my_error_user':body})
            subject='Complete your registration'
            token=uname.encode()
            #token=base64.b64encode(token)
            #token=uname
            token=base64.urlsafe_b64encode(token)
            message="Hi %s click on the following link to confirm your registration. The Link is :http://127.0.0.1:8000/activate/?token=%s" %(uname,token)
            email=EmailMessage(subject,message,to=[email_id])
            email.send()
            a=User.objects.create_user(username=uname,password=pwd,first_name=firstname,last_name=lastname,email=email_id)
            a.is_active=False
            a.save()
            b=UserProfile.objects.create(user=a,profile_pic=profile_image,phone=phone_no,dob=dob_date)
            #return HttpResponse("An email has been sent to ur email id !")
            body="An email has been sent to ur email id ! Click on that link to complete your registration !! "
            return render(request, 'account/message.html', {'message': body})
        #print(form.errors.as_data())
        #return HttpResponse(form.errors.as_data())
        print("Not valid")
        return render(request, 'account/signup.html', {'form': form})
    #html = r'''<html> <head> <title> Blog Is Not valid !! </title> </head> <body > <center> Blog Is Not valid!! Type all the required fields !! </center><hr> <a href="http://127.0.0.1:8000/blog/addblog/"> Click here to Create blog  Again !! </a> </body> </html>'''
    #return HttpResponse(html)
    body="Invalid Details !! Try again !!  "
    return render(request, 'account/message.html', {'message': body})

def change_password_url(request):
    #a=request.get_full_path()
    #a="http://127.0.0.1:8000/reset/?username=dceshubh/?password=pbkdf2_sha256$15000$NYC1dfbyW7w4$aCl2i8oJOqUl2uyY86aeDt30QUtm/AaWiLuq82AAcd0="
    global a
    if (request.method=='GET'):
        if "username" not in request.get_full_path() or "password" not in request.get_full_path():
            body="4041 Page Not Found !!"
            return render(request,'account/message.html',{'message':body})
        uname=request.GET.get('username')
        pwd=request.GET.get('password')
        if uname is None or pwd is None:
            body="4042 Page Not Found !!"
            return render(request,'account/message.html',{'message':body})
        print(uname)
        pwd=pwd.split("'")[-1]
        print(pwd)
        pwd=pwd.encode()
        pwd=base64.urlsafe_b64decode(pwd)
        print(pwd)
        pwd=pwd.decode()
        #pwd.decode()
        print(pwd)
        uname=uname.split("'")[-2]
        uname=uname.encode()
        print(uname)
        uname=base64.urlsafe_b64decode(uname)
        uname=uname.decode()
        flag=False
        list=User.objects.all()
        for item in list:
            if uname==item.username:
                flag=True
                break
        if flag==False:
            body="4043 Page Not Found !!"
            return render(request,'account/message.html',{'message':body})
        a=User.objects.get(username=uname)
        print(pwd)
        print(a.password)
        if pwd!=a.password:
            body="4044 Page Not Found !!"
            return render(request,'account/message.html',{'message':body})
        form=PasswordChange()
        return render(request, 'account/passwordchange.html', {'form': form,'name':uname})
    else:
        form=PasswordChange(request.POST)
        if form.is_valid():
            pwd=form.cleaned_data['password']
            pwd1=form.cleaned_data['password1']
            if pwd!=pwd1:
                body="Passwords didnot match !!"
                return render(request,'account/passwordchange.html',{'form':form,'name':a.username,'my_error':body})
            a.set_password(pwd)
            a.save()
            body="Your Password has been Reset !! You Can LOg In and Enjoy !!"
            return render(request,'account/message.html',{'message':body})
        else:
            return render(request,'account/passwordchange.html',{'form':form,'name':a.username})


def authenticate_user(request):
    #a=request.get_full_path()
    #print(a)
    #return HttpResponse(a)
    '''if "activate/b'" not in a:
        body="404 Page Not Found !!"
        return render(request,'account/message.html',{'message':body})

    name=a.split("'")[-1]
    name=name.encode()
    #name=name+"'"
    print(name)
    #return HttpResponse(name)'''
    name=request.GET.get('token')
    #name=name+"'"
    print(name)
    #name=name.append("'")
    #name=name+"'"
    #name=name.encode()
    name=name.split("'")[-1]
    print(name)
    name=name.encode()
    print(name)
    try:
        #name=base64.b64decode(name)
        b=base64.urlsafe_b64decode(name)
        print(b)
    except binascii.Error:
        body="404 Page Not Found !!"
        return render(request,'account/message.html',{'message':body})

    #return HttpResponse(name)
    print(b)
    name=b.decode()
    print(name)
    flag=False
    list=User.objects.all()
    for item in list:
        if name==item.username:
            flag=True
            break
    if flag==False:
        body="404 Page Not Found !!"
        return render(request,'account/message.html',{'message':body})
    obj=User.objects.get(username=name)
    if obj.is_active==True:
        #return HttpResponse("You Have already registered !!")
        body="Dear %s , You have already registered ! Kindly Log In !! " %(obj.username)
        return render(request, 'account/message.html', {'message': body})
    obj.is_active=True
    #obj.is_staff=True
    #obj.is_superuser=True
    obj.save()
    body="Dear %s, You Have successfully completed your registration on our website !! You can now Log In !!" %(obj.username)
    return render(request,'account/message.html',{'message':body})
    #return HttpResponse(obj.username)

def forget_password(request):
    if (request.user.is_authenticated()):
        return redirect('/feeds/')
    if (request.method == 'POST'):
        form = ForgetPasswordForm(request.POST)
        if form.is_valid():
            uname = form.cleaned_data['username']
            flag=False
            list=User.objects.all()
            for item in list:
                if uname==item.username:
                    flag=True
                    break
            if flag==False:
                body="You have entered an invalid username. !! Kindly enter the Correct Username !! "
                return render(request,'account/message.html',{'message':body})
            a=User.objects.get(username=uname)
            #name=uname.encode()
            #new_pwd=base64.b64encode(name)
            #new_pwd=new_pwd.decode()
            #a.set_password(new_pwd)
            pwd=a.password
            pwd=pwd.encode()
            pwd=base64.urlsafe_b64encode(pwd)
            uname_coded=uname.encode()
            uname_coded=base64.urlsafe_b64encode(uname_coded)
            subject="Your Password Reset Link "
            #message="Hi %s This is your new password %s " %(uname,new_pwd)
            message="Hi %s click on the following link to reset your password. The Link is :http://127.0.0.1:8000/reset/?username=%s&password=%s" %(uname,uname_coded,pwd)
            email=EmailMessage(subject,message,to=[a.email])
            email.send()
            a.save()
            #html = r'''<html> <head> <title> Password sent successfully !! </title> </head> <body > <center> Password has been sent Successfully  to your registered enail id !! !! </center><hr> </body> </html>'''
            #return HttpResponse(html)
            body="Password Reset Link has been sent Successfully  to your registered email id !! !! "
            return render(request, 'account/message.html', {'message': body})
        else:
            #body="Enter the Correct Username !! "
            return render(request,'account/forgetpwd.html',{'form':form})
    #html = r'''<html> <head> <title> Blog Is Not valid !! </title> </head> <body > <center> Blog Is Not valid!! Type all the required fields !! </center><hr> <a href="http://127.0.0.1:8000/blog/addblog/"> Click here to Create blog  Again !! </a> </body> </html>'''
    #return HttpResponse(html)
    else:
        form = ForgetPasswordForm()
        return render(request, 'account/forgetpwd.html', {'form': form})


def resend_link(request):
    if (request.user.is_authenticated()):
        return redirect('/feeds/')
    if (request.method == 'POST'):
        form = ResendForm(request.POST)
        if form.is_valid():
            uname = form.cleaned_data['username']
            flag=False
            list=User.objects.all()
            for item in list:
                if uname==item.username:
                    flag=True
                    break
            if flag==False:
                body="You have entered an invalid username. !! Kindly enter the Correct Username !! "
                return render(request,'account/message.html',{'message':body})
            a=User.objects.get(username=uname)
            subject='Complete your registration'
            token=uname.encode()
            token=base64.b64encode(token)
            #token=uname
            email_id=a.email
            message="Hi %s click on the following link to confirm your registration. The Link is :http://127.0.0.1:8000/activate/%s " %(uname,token)
            email=EmailMessage(subject,message,to=[email_id])
            email.send()
            #a=User.objects.create_user(username=uname,password=pwd,first_name=firstname,last_name=lastname,email=email_id)
            a.is_active=False
            a.save()
            #return HttpResponse("An email has been sent to ur email id !")
            body="An email has been Re-sent to ur email id ! Click on that link to complete your registration !! "
            return render(request, 'account/message.html', {'message': body})
        else:
            #body="Enter the Correct Username !! "
            return render(request,'account/resend.html',{'form':form})
    #html = r'''<html> <head> <title> Blog Is Not valid !! </title> </head> <body > <center> Blog Is Not valid!! Type all the required fields !! </center><hr> <a href="http://127.0.0.1:8000/blog/addblog/"> Click here to Create blog  Again !! </a> </body> </html>'''
    #return HttpResponse(html)
    else:
        form = ResendForm()
        return render(request, 'account/resend.html', {'form': form})

def search_user(request):
    value=request.GET.get('term')
    my_list=User.objects.filter(username__istartswith=value)
    return HttpResponse(my_list)


def autocomplete(request):
    term=request.GET.get('term','')
    if term=='':
        data=[]
    else:
        users=User.objects.filter(username__icontains=term)
        '''data=[{'label':user.get_full_name(),'value':user.id} for user in users]
        data=serializers.serialize("json",data)'''
        data=serializers.serialize("json",users,fields=('id','first_name','last_name','username'))
        #return HttpResponse(json.dumps(data),content_type='applications/json')
