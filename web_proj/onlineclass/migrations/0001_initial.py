# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Progress',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('last_quiz', models.IntegerField(default=0)),
                ('last_tutorial', models.IntegerField(default=0)),
                ('user', models.OneToOneField(to='account.UserProfile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='question',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('ques_text', models.CharField(max_length=500)),
                ('ques_ans', models.CharField(max_length=1)),
                ('option1', models.CharField(max_length=300)),
                ('option2', models.CharField(max_length=300)),
                ('option3', models.CharField(max_length=300)),
                ('option4', models.CharField(max_length=300)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='quiz',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('quiz_num', models.CharField(max_length=2)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='tutorials',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('name', models.CharField(max_length=300)),
                ('text', models.CharField(max_length=10000)),
                ('file', models.FileField(upload_to='/tutorials')),
                ('quizz', models.OneToOneField(to='onlineclass.quiz')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='question',
            name='quiz',
            field=models.ForeignKey(to='onlineclass.quiz'),
            preserve_default=True,
        ),
    ]
