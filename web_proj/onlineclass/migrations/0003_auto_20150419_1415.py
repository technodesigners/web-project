# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('onlineclass', '0002_auto_20150419_1324'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tutorials',
            name='file',
            field=models.FileField(upload_to='tutorials/'),
            preserve_default=True,
        ),
    ]
