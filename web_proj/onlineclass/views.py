from django.shortcuts import render,redirect
from django.http import HttpResponse,Http404
import datetime
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from onlineclass.models import tutorials,quiz,question
from .models import *

# Create your views here.
@login_required(login_url = '/login/')
def quiz1(request):
	quiz1=quiz.objects.get(quiz_num=1)
	ques1=quiz1.question_set.all()
	print(ques1)
	return render(request, 'onlineclass/quiz.html', {'quiz':quiz1,'ques':ques1})

@login_required(login_url = '/login/')
def quiz2(request):
	quiz1=quiz.objects.get(quiz_num=2)
	ques1=quiz1.question_set.all()
	print(ques1)
	return render(request, 'onlineclass/quiz.html', {'quiz':quiz1,'ques':ques1})

@login_required(login_url = '/login/')
def quiz3(request):
	quiz1=quiz.objects.get(quiz_num=3)
	ques1=quiz1.question_set.all()
	print(ques1)
	return render(request, 'onlineclass/quiz.html', {'quiz':quiz1,'ques':ques1})

@login_required(login_url = '/login/')
def quiz4(request):
	quiz1=quiz.objects.get(quiz_num=4)
	ques1=quiz1.question_set.all()
	print(ques1)
	return render(request, 'onlineclass/quiz.html', {'quiz':quiz1,'ques':ques1})

@login_required(login_url = '/login/')
def quiz5(request):
	quiz1=quiz.objects.get(quiz_num=5)
	ques1=quiz1.question_set.all()
	print(ques1)
	return render(request, 'onlineclass/quiz.html', {'quiz':quiz1,'ques':ques1})

@login_required(login_url = '/login/')
def quiz6(request):
	quiz1=quiz.objects.get(quiz_num=6)
	ques1=quiz1.question_set.all()
	print(ques1)
	return render(request, 'onlineclass/quiz.html', {'quiz':quiz1,'ques':ques1})

@login_required(login_url = '/login/')
def quiz7(request):
	quiz1=quiz.objects.get(quiz_num=7)
	ques1=quiz1.question_set.all()
	print(ques1)
	return render(request, 'onlineclass/quiz.html', {'quiz':quiz1,'ques':ques1})

@login_required(login_url = '/login/')
def quiz8(request):
	quiz1=quiz.objects.get(quiz_num=8)
	ques1=quiz1.question_set.all()
	print(ques1)
	return render(request, 'onlineclass/quiz.html', {'quiz':quiz1,'ques':ques1})

@login_required(login_url = '/login/')
def dashboard(request):
	x=request.user.userprofile
	tutorial_list=tutorials.objects.all()
	return render(request, 'onlineclass/dashboard.html', {'tutorial':tutorial_list,'userprofile':x})

def quiz_show1(request,quiz_no):
    if request.method=='POST':
        quize=quiz.objects.get(quiz_num=int(quiz_no))
        questions=question.objects.filter(quiz=quize)
        score=0;
        correct=0
        wrong=0
        unattempted=0
        k=questions[0].pk

        for i in questions:
                if request.POST[str(k)]==i.ques_ans:
                        print(type(i.ques_ans))
                        score=score+4
                        correct+=1
                elif request.POST[str(k)]=='0':
                        unattempted += 1
                else:
                        score=score-1
                        wrong+=1
                k+=1
        users=request.user
        a=users.userprofile
        if score>12:
                t=Progress.objects.filter(user=users)
                #t.update(last_quiz=F('last_quiz') + 1)
                a.score+=score
                flag=1
        else:
                flag=0
        a.save()
        print(a.score)
        return render(request, 'account/result.html', {'quiz':quize,'correct':correct,'incorrect':wrong,'unattempted':unattempted,'score':score,'flag':flag})
