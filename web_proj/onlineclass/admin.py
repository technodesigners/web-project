from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django_markdown.admin import MarkdownModelAdmin
from django_markdown.widgets import AdminMarkdownWidget
from django.db.models import TextField
from .models import *

# Register your models here.
class EntryAdmin(MarkdownModelAdmin):
	list_display = ("name",)
	formfield_overrides = {TextField: {'widget': AdminMarkdownWidget}}

class QuestionAdmins(admin.ModelAdmin):
	list_display = ('quiz_num' ,)

admin.site.register(tutorials,EntryAdmin)
admin.site.register(Progress)
admin.site.register(quiz, QuestionAdmins)
admin.site.register(question)