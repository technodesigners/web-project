from django.db import models
from django.contrib.auth.models import User
from account.models import UserProfile

# Create your models here.
class Progress(models.Model):
    last_quiz=models.IntegerField(default=0)
    last_tutorial=models.IntegerField(default=0)
    user=models.OneToOneField(UserProfile)

class quiz(models.Model):
    quiz_num=models.CharField(max_length=2)
    def __self__(self):
        return self.quiz_num

class tutorials(models.Model):
    name = models.CharField(max_length=300)
    text = models.TextField()
    quizz=models.OneToOneField(quiz)
    file=models.FileField(upload_to='tutorials/')
    def __self__(self):
        return self.name

class question(models.Model):
    ques_text=models.CharField(max_length=500)
    ques_ans=models.CharField(max_length=1)
    quiz=models.ForeignKey(quiz)
    option1=models.CharField(max_length=300)
    option2=models.CharField(max_length=300)
    option3=models.CharField(max_length=300)
    option4=models.CharField(max_length=300)
    def __self__(self):
        return self.ques_text
