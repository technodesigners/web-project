import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'web_proj.settings')

import django
django.setup()
from onlineclass.models import quiz,question
def populate():





    #For quiz #1
    t=add_quiz(1)
    ques_text="In Linux Operating System, everything is a "
    ques_ans=4
    option1="Process"
    option2="Hardware"
    option3="Signal"
    option4="File"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="Which of the freedom Linux operating system doesn’t provide "
    ques_ans=4
    option1="The freedom to distribute software and source code"
    option2="The ability to modify and create derived works"
    option3="Integrity of author's code"
    option4="Guarantee to provide the software for free"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="What is relation between Unix and Linux "
    ques_ans=1
    option1="Linux is a derived from Unix"
    option2="Unix is derived from Linux"
    option3="Unix and Linux are derived from same family of OS"
    option4="Both Unix and Linux are proprietary Software"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="How to get help of cat command?"
    ques_ans=3
    option1="help cat"
    option2="cat help"
    option3="cat -help"
    option4="None of these"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="How do you get help about the command “\cp”\?"
    ques_ans=1
    option1="help cp"
    option2="man cp"
    option3="cp ?"
    option4="?cp"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="Which of the following command is used to list the running process? "
    ques_ans=1
    option1="ps"
    option2="ls"
    option3="df"
    option4="none"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="How would you display short usage summary of any command"
    ques_ans=1
    option1="whatis command"
    option2="info command"
    option3="man command"
    option4="whereis command"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="Which of the following command is syntactically incorrect "
    ques_ans=1
    option1="man -help"
    option2="man --help"
    option3="man -k"
    option4="man"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="Bourne-again-shells of the operating system are used by"
    ques_ans=4
    option1="Windows"
    option2="Unix"
    option3="Linux"
    option4="Both b and c"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="The part of the Unix operating system that interacts with the hardware is called "
    ques_ans=2
    option1="GNU project"
    option2="The Kernel"
    option3="The shell"
    option4="Linux"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    #QUiz 2
    t=add_quiz(2)
    ques_text="Which command is used to list all the files in your current directory (including hidden)?"
    ques_ans=1
    option1="ls -l"
    option2="ls -t"
    option3="ls -a"
    option4="ls -i"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="Which option of rmdir command will remove all directories a, b, c if path is a/b/c"
    ques_ans=3
    option1="-b"
    option2="-o"
    option3="-p"
    option4="-t"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="If a file is removed in Unix using ‘rm’ then"
    ques_ans=2
    option1="The file can be recovered by a normal user"
    option2="The file cannot be recovered by a user"
    option3="The file can be fully recovered provided the sytem is not rebooted"
    option4="The file will be moved to /lost+found directory and can be recovered only by administrator’s intervention"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="Executing the ‘cd ..’ command when at the root level causes "
    ques_ans=4
    option1="Error message indicating the user can’t access beyond the root level"
    option2="Behavior is unix-flavor dependent"
    option3="Results in changing to the ‘home’ directory"
    option4="Nothing happens"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text=" How do you rename file “new” to file “old”? "
    ques_ans=1
    option1="mv new old"
    option2="move new old"
    option3="cp new old"
    option4="rn new old"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="What command is used to copy files and directories? "
    ques_ans=2
    option1="copy"
    option2="cp"
    option3="rn"
    option4="cpy"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="Where can I find the printer in the file structure? "
    ques_ans=2
    option1="/etc"
    option2="/dev"
    option3="/lib"
    option4="/printer"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="Which of the following statement is true? "
    ques_ans=3
    option1="The cp command will preserve the meta data of the file"
    option2="The sort command by default sorts in the numeric order"
    option3="The mv command will preserve the meta data of the file"
    option4="The command ps will display the filesystem usage"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="What UNIX command is used to update the modification time of a file?"
    ques_ans=4
    option1="time"
    option2="modify"
    option3="cat"
    option4="touch"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="Which command is used to display all the files including hidden files in your current and its subdirectories? "
    ques_ans=1
    option1="ls -aR"
    option2="ls -a"
    option3="ls -R"
    option4="ls -l"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()






    #quiz #3
    t=add_quiz(3)
    ques_text="Which command runs the shell built-in command ‘command’ with the given argument?"
    ques_ans=4
    option1="builtin"
    option2="caller"
    option3="there is no command present for this purpose"
    option4="none of the mentioned"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="Which command is used to reexecute the previous command? "
    ques_ans=1
    option1="!!"
    option2="!cat"
    option3="!3"
    option4="!$"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="Shell is ?"
    ques_ans=1
    option1="Command Interpreter"
    option2="Interface between Kernel and Hardware"
    option3="Interface between user and applications"
    option4="Command Compiler"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="Which of the following is true?"
    ques_ans=3
    option1="Shell is a process and can be started by superuser only"
    option2="Shell is a built-in Kernel functionality"
    option3="Shell is a wrapper for all the commands and utilities"
    option4="None of the mentioned"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="BASH shell stands for?"
    ques_ans=1
    option1="Bourne-again Shell"
    option2="Basic Access Shell"
    option3="Basic to Advanced Shell"
    option4="Big & Advanced Shell"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="Which is true with regards to the shell prompt"
    ques_ans=3
    option1="It can be accidentally erased with backspace"
    option2="The prompt cannot be modified"
    option3="The prompt can be customized (modified)"
    option4="None of the mentioned"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text='''What would be the current working directory at the end of the following command sequence?
    $ pwd
    /home/user1/proj
    $ cd  src
    $ cd  generic
    $ cd  .
    pwd
    '''
    ques_ans=4
    option1="/home/user1/proj"
    option2="/home/user1/proj/src"
    option3="/home/user1"
    option4="/home/user1/proj/src/generic"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="Which command is used to execute the last command you have executed? "
    ques_ans=3
    option1="!!"
    option2="!-1"
    option3="Both A and B"
    option4="!-2"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="Which command is used to print the current working direcotry? "
    ques_ans=3
    option1="pwd"
    option2="echo $PWD"
    option3="Both A and B"
    option4="cwd"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="Which of the following command return the exit status of last command?"
    ques_ans=3
    option1="$!"
    option2="$$"
    option3="$?"
    option4="$#"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    #For quiz #4
    t=add_quiz(4)
    ques_text="The redirection 2> abc implies "
    ques_ans=3
    option1=" Write file 2 to file abc"
    option2="Write standard output to abc"
    option3="Write standard error to abc"
    option4="none of the mentioned"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="cmd 2>&1 > abc will "
    ques_ans=4
    option1="Write file2 to file1"
    option2="Write standard output and standard error to abc"
    option3="Write standard error to abc"
    option4="Write standard output to abc & standard error to monitor"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="cmd > abc 2>&1 will"
    ques_ans=2
    option1="Write file2 to file1"
    option2="Write standard output and standard error to abc"
    option3=" Write standard error to abc"
    option4="Write standard output to abc & standard error to monitor"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()

    ques_text="Which of these is the correct method for appending “foo” in /tmp/bar file?"
    ques_ans=2
    option1="echo foo > /tmp/bar"
    option2="echo foo >> /tmp/bar"
    option3="echo foo | /tmp/var"
    option4="/tmp/bar < echo foo"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()

    ques_text="Syntax to suppress the display of command error to monitor?"
    ques_ans=4
    option1=" command > &2"
    option2="command 2> &1"
    option3=" command 2> &2"
    option4="command 2> /dev/null"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()

    ques_text="""The following commands gives the output like this
    #cat file1 file2
    #cat: file1: No such file or directory
    hello
    If we execute the command “cat file1 file2 1>2 2>&1” the output would be"""
    ques_ans=2
    option1="cat: file1: No such file or directory hello"
    option2="No output is displayed"
    option3="Cat: 1>2: No such file or directory"
    option4= "hello"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()

    ques_text="cat < file1 >> file2 | file3"
    ques_ans=4
    option1="file1 content will be appended to file2 and finally stored in file3"
    option2="file1 content will be appended to file2 and file3 will be ignored"
    option3="file2 and file3 will have same content"
    option4="syntax error"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()

    ques_text="Executing cat /etc/password > /dev/sda as superuser will"
    ques_ans=2
    option1="Write data into a regular file called /dev/sda"
    option2="Write data to the physical device sda"
    option3="Create a temporary file /dev/sda and write data to it"
    option4=" None of the above"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()

    ques_text="""From where would the read statement read if the following statements were executed?
    exec < file1
    exec < file2
    exec < file3
    read line"""
    ques_ans=2
    option1="It would read all the files"
    option2="It would not read any files"
    option3=" It would read all the files in reverse order"
    option4=" It would read only file3"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()

    ques_text="To feed standard output of one command to standard input of another in a single shell session"
    ques_ans=3
    option1="IO redirection can be used"
    option2="Named pipes can be used"
    option3="The pipe operator provided by the shell can be used"
    option4="It can  not be done"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()


















    #quiz #5
    t=add_quiz(5)
    ques_text="Which one of the following is a mount point for a temporarily mounted filesystem?"
    ques_ans=1
    option1="/mnt directory"
    option2="/media directory"
    option3="/dev directory"
    option4="none of the mentioned"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="Which command is sued to unmount the file system?"
    ques_ans=2
    option1="unmount"
    option2="umount"
    option3="df"
    option4="ps"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="The command used to search the file is"
    ques_ans=3
    option1="grep"
    option2="search"
    option3="find"
    option4=":q!"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    #mcq #6
    t=add_quiz(6)
    ques_text="If a program executing in background attempts to read from STDIN"
    ques_ans=2
    option1="It is terminated"
    option2="It’s execution is suspended"
    option3="STDIN is made available to it"
    option4="None of the mentioned"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="Which command is used to bring the background process to forground?"
    ques_ans=2
    option1="bg"
    option2="fg"
    option3="background"
    option4="foreground"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="How to run a process in the background?"
    ques_ans=1
    option1="&"
    option2="*"
    option3="?"
    option4="|"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="Which command can be executed by a user who is already logged into the system, in order to change to the root user? (type the command without any parameters)"
    ques_ans=1
    option1="su"
    option2="root"
    option3="chroot"
    option4="user"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="Process information in the current shell can be obtained by using"
    ques_ans=4
    option1="kill"
    option2="bg"
    option3="fg"
    option4="ps"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="Which signal is sent by the command \"kill -9 \" ?"
    ques_ans=3
    option1="INT"
    option2="TERM"
    option3="KILL"
    option4="STOP"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text='''A user issues the following command sequence:
    $ a.out &
    $ bash
    $ a.out &
    If the user kills the bash process, then which of the following is true?
'''
    ques_ans=4
    option1="The second a.out process is also terminated"
    option2="The second a.out process becomes a defunct process"
    option3="The first a.out process becomes a zombie process"
    option4="init process becomes parent of second a.out process"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="The signal sent to a process when the Ctrl-C key is pressed is"
    ques_ans=4
    option1="KILL"
    option2="TSTP"
    option3="TERM"
    option4="INT"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="We can change the priority of a running process using"
    ques_ans=2
    option1="nice"
    option2="renice"
    option3="priority cannot be changed for a running process"
    option4="only superuser can change the priority"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="nohup is used to"
    ques_ans=2
    option1="automatically hang up the process after logout"
    option2="continue the process after logout"
    option3="create backgroung process"
    option4="manually hang up the process after logout"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    #QUIZ #7
    t=add_quiz(7)
    ques_text="A user does a chmod operation on a file. Which of the following is true?"
    ques_ans=3
    option1="The last accessed time of the file is updated"
    option2="The last modification time of the file is updated"
    option3="The last change time of the file is updated"
    option4="None of the above"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()

    ques_text="What is the command to set the execute permissions to all the files and subdirectories within the directory /home/user1/direct "
    ques_ans=2
    option1="chmod –r +x /home/user1/direct"
    option2="chmod –R +x /home/user1/direct"
    option3="chmod –f –r +x /home/user1/direct"
    option4=" chmod –F +x /home/user1/direct"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()

    ques_text="Which command is used to assign read-write permission to the owner? "
    ques_ans=3
    option1="chmod a+r file"
    option2="chmod o+r file"
    option3="chmod u=rw file"
    option4="chmod og-r file"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()

    ques_text="""Given the command
              $ chmod o-w datafile"""
    ques_ans=4
    option1="sets write permission to everyone for datafile"
    option2="sets write permission to others for datafile"
    option3="clears write permission to everyone for datafile"
    option4=" clears write permission to others for datafile"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()

    ques_text="If you are a root user, how can you grand execute permission only for the owner of the file project1?"
    ques_ans=2
    option1="chmod +x project1"
    option2="chmod u+x project1"
    option3=" chmod a+x project1"
    option4="chmod U+X project1"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()

    ques_text=""" A user executes the following command successfully:
    $ chmod +x file1.txt
    Which of the following is true of the output of this command?"""
    ques_ans=4
    option1=" The command results in adding execute permission to the user who ran this command"
    option2="The command results in adding execute permission for the owner of the file"
    option3="The command results in an error since the file is not an executable file"
    option4="The command results in adding execute permission for all users (i.e., user,group & others)"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()

    ques_text="What does chmod +t do?"
    ques_ans=4
    option1=" wrong syntax"
    option2="set effective userid for filename"
    option3="set effective groupid for filename"
    option4=" set the sticky bit"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()

    ques_text="The permission -rwxr-xr-t represented in octal expression will be"
    ques_ans=2
    option1="0777"
    option2="1755"
    option3="1754"
    option4="2754"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()

    ques_text="Which of the following umask settings allow execute permission to be set by default on regular files"
    ques_ans=4
    option1=" 222"
    option2=" 111"
    option3="000"
    option4=") None of the given choices"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()

    ques_text="The command chmod 4777 a.out "
    ques_ans=1
    option1="will set the suid bit of a.out"
    option2="will set the suid bit of a.out only if the command is issued by root"
    option3=" is not a valid command"
    option4="will set the sticky bit of a.out"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()


    #quiz number 8
    t=add_quiz(8)
    ques_text="A user does a chmod operation on a file. Which of the following is true?"
    ques_ans=1
    option1="The last accessed time of the file is updated"
    option2="The last modification time of the file is updated"
    option3="The last change time of the file is updated"
    option4="None of the above"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="What is the command to set the execute permissions to all the files and subdirectories within the directory /home/user1/direct"
    ques_ans=1
    option1="chmod –r +x /home/user1/direct"
    option2="chmod –R +x /home/user1/direct"
    option3="chmod –f –r +x /home/user1/direct"
    option4="chmod –F +x /home/user1/direct"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="Which command is used to assign read-write permission to the owner?"
    ques_ans=1
    option1="chmod a+r file"
    option2="chmod o+r file"
    option3="chmod u=rw file"
    option4="chmod og-r file"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text='''Given the command
    $ chmod o-w datafile
    '''
    ques_ans=4
    option1="sets write permission to everyone for datafile"
    option2="sets write permission to others for datafile"
    option3="clears write permission to everyone for datafile"
    option4="clears write permission to others for datafile"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="If you are a root user, how can you grand execute permission only for the owner of the file project1?"
    ques_ans=4
    option1="chmod +x project1"
    option2="chmod u+x project1"
    option3="chmod a+x project1"
    option4="chmod U+X project1"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text='''A user executes the following command successfully:
$ chmod +x file1.txt
Which of the following is true of the output of this command?
'''
    ques_ans=2
    option1="The command results in adding execute permission to the user who ran this command"
    option2="The command results in adding execute permission for the owner of the file"
    option3="The command results in an error since the file is not an executable file"
    option4="The command results in adding execute permission for all users (i.e., user,group & others)"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="What does chmod +t do?"
    ques_ans=3
    option1="wrong syntax"
    option2="set effective userid for filename"
    option3="set effective groupid for filename"
    option4="set the sticky bit"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="The permission -rwxr-xr-t represented in octal expression will be"
    ques_ans=3
    option1="0777"
    option2="1755"
    option3="1754"
    option4="2754"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="Which of the following umask settings allow execute permission to be set by default on regular files"
    ques_ans=2
    option1="222"
    option2="111"
    option3="000"
    option4="None of the above"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()
    ques_text="The command chmod 4777 a.out"
    ques_ans=3
    option1="will set the suid bit of a.out"
    option2="will set the suid bit of a.out only if the command is issued by root"
    option3="is not a valid command"
    option4="will set the sticky bit of a.out"
    m=question.objects.get_or_create(quiz=t,ques_text=ques_text)[0]
    m.ques_text=ques_text
    m.ques_ans=ques_ans
    m.option1=option1
    m.option2=option2
    m.option3=option3
    m.option4=option4
    m.save()


def add_quiz(no):
	p = quiz.objects.get_or_create(quiz_num=no)[0]
	return p

# Start execution here
if __name__ == '__main__':
    print ("Starting Question population script...")
    populate()
